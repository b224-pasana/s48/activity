// alert("hello")


let posts = [];

let count = 1;



    document.querySelector("#form-add-post").addEventListener("submit", (e) => {

        e.preventDefault();

        posts.push({
            id: count,
            title: document.querySelector("#txt-title").value,
            body: document.querySelector("#txt-body").value
        });

        count++
        console.log(posts);
        alert("Movie Successfully Added!");

        showPosts();

    });

    // RETRIEVE POST 
    const showPosts = () => {

        // Create a variable that will contain all the posts
        let postEntries = "";
    
        // We will use forEach() to display each movie inside our mock database.
        posts.forEach((post) => {
    
            postEntries += `
                <div id="post-${post.id}">
                    <h3 id="post-title-${post.id}">${post.title}</h3>
                    <p id="post-body-${post.id}">${post.body}</p>
                    <button onclick="editPost(${post.id})">Edit</button>
                    <button type="reset" id="post-delete-${post.id}" onclick="deletePost(${post.id})">Delete</button>
                </div>
            `
        });
    
        // To check what is stored in the postEntries variables.
        console.log(postEntries);
    
        // To assign the value of postEntries to the element with "div-post-entries" id.
        document.querySelector("#div-post-entries").innerHTML = postEntries
    
    };
    
    
    // EDIT POST DATA / Edit Button
    
    const editPost = (id) => {
    
        let title = document.querySelector(`#post-title-${id}`).innerHTML;
        let body = document.querySelector(`#post-body-${id}`).innerHTML;
    
        document.querySelector("#txt-edit-id").value = id;
        document.querySelector("#txt-edit-title").value = title;
        document.querySelector("#txt-edit-body").value = body;
    };
    
    document.querySelector("#form-edit-post").addEventListener("submit", (e) => {
        e.preventDefault();
    
        for (let i = 0; i < posts.length; i++) {
    
            if(posts[i].id.toString() === document.querySelector("#txt-edit-id").value) {
                posts[i].title = document.querySelector("#txt-edit-title").value;
                posts[i].body = document.querySelector("#txt-edit-body").value
    
                showPosts(posts);
                alert("Successfully updated!");
    
                break;
            }
        }
    });

    // Function to DELETE POST 
    const deletePost = (id) => {
        
        for (let i = 0; i < posts.length; i++) {
            let obj = posts[i];

            if (obj.id === id) {

                posts.splice(i, 1);
                showPosts(posts);
                break;
            }
        }
        
    };
 